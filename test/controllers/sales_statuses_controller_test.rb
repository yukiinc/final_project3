require 'test_helper'

class SalesStatusesControllerTest < ActionController::TestCase
  setup do
    @sales_status = sales_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sales_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sales_status" do
    assert_difference('SalesStatus.count') do
      post :create, sales_status: { status_type: @sales_status.status_type }
    end

    assert_redirected_to sales_status_path(assigns(:sales_status))
  end

  test "should show sales_status" do
    get :show, id: @sales_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sales_status
    assert_response :success
  end

  test "should update sales_status" do
    patch :update, id: @sales_status, sales_status: { status_type: @sales_status.status_type }
    assert_redirected_to sales_status_path(assigns(:sales_status))
  end

  test "should destroy sales_status" do
    assert_difference('SalesStatus.count', -1) do
      delete :destroy, id: @sales_status
    end

    assert_redirected_to sales_statuses_path
  end
end
