class Sale < ActiveRecord::Base
  belongs_to :quote
  belongs_to :sales_status
  belongs_to :employee
  belongs_to :customer
end
