class Quote < ActiveRecord::Base
  belongs_to :quote_status
  has_many :sales
  belongs_to :car
end
