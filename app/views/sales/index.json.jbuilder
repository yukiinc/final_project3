json.array!(@sales) do |sale|
  json.extract! sale, :id, :quote_id, :customer_id, :employee_id, :sale_status_id, :date, :total
  json.url sale_url(sale, format: :json)
end
