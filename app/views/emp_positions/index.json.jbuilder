json.array!(@emp_positions) do |emp_position|
  json.extract! emp_position, :id, :position_desc
  json.url emp_position_url(emp_position, format: :json)
end
