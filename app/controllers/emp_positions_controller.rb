class EmpPositionsController < ApplicationController
  before_action :set_emp_position, only: [:show, :edit, :update, :destroy]

  # GET /emp_positions
  # GET /emp_positions.json
  def index
    @emp_positions = EmpPosition.all
  end

  # GET /emp_positions/1
  # GET /emp_positions/1.json
  def show
  end

  # GET /emp_positions/new
  def new
    @emp_position = EmpPosition.new
  end

  # GET /emp_positions/1/edit
  def edit
  end

  # POST /emp_positions
  # POST /emp_positions.json
  def create
    @emp_position = EmpPosition.new(emp_position_params)

    respond_to do |format|
      if @emp_position.save
        format.html { redirect_to @emp_position, notice: 'Emp position was successfully created.' }
        format.json { render :show, status: :created, location: @emp_position }
      else
        format.html { render :new }
        format.json { render json: @emp_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emp_positions/1
  # PATCH/PUT /emp_positions/1.json
  def update
    respond_to do |format|
      if @emp_position.update(emp_position_params)
        format.html { redirect_to @emp_position, notice: 'Emp position was successfully updated.' }
        format.json { render :show, status: :ok, location: @emp_position }
      else
        format.html { render :edit }
        format.json { render json: @emp_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emp_positions/1
  # DELETE /emp_positions/1.json
  def destroy
    @emp_position.destroy
    respond_to do |format|
      format.html { redirect_to emp_positions_url, notice: 'Emp position was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emp_position
      @emp_position = EmpPosition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def emp_position_params
      params.require(:emp_position).permit(:position_desc)
    end
end
