class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :make_id
      t.integer :year_id
      t.integer :color_id
      t.string :vin
      t.decimal :price

      t.timestamps null: false
    end
  end
end
