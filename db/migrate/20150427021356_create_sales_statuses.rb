class CreateSalesStatuses < ActiveRecord::Migration
  def change
    create_table :sales_statuses do |t|
      t.string :status_type

      t.timestamps null: false
    end
  end
end
